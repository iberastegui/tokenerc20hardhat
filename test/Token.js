const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("My Token Contract", function () {
  let token, owner, addr1, addr2;

  beforeEach(async function () {
    const Token = await ethers.getContractFactory("Token");
    [owner, addr1, addr2] = await ethers.getSigners();
    token = await Token.deploy(owner.address);
    await token.deployed();
  });

  describe("Deployment", function () {
    it("Should set the right owner", async function () {
      expect(await token.owner()).to.equal(owner.address);
    });

    it("Should assign the total supply of tokens to the owner", async function () {
      const ownerBalance = await token.balanceOf(owner.address);
      expect(await token.totalSupply()).to.equal(ownerBalance);
    });

    it("Should deploy with 500000 of supply for the owner of the contract", async function () {
      const decimals = await token.decimals();
      const balance = await token.balanceOf(owner.address);
      expect(ethers.utils.formatEther(balance)).to.equal("500000.0");
    });
  });

  describe("Transactions", function () {
    it("Should let you send tokens to another address", async function () {
      //note for myself
      //ethers.utils.formatEther for read balance //clean the number with decimals to normal format from 500.000.000.000.000.000.000.000 to 500000.0
      //ethers.utils.parseEther to send balance // eg 100 is converted to 100 +N decimals (100.000.000.000.000.000.000) in this case +18 decimals
      await token.transfer(addr1.address, ethers.utils.parseEther("10"));

      expect(await token.balanceOf(addr1.address)).to.equal(
        ethers.utils.parseEther("10")
      );
    });

    it("Should fail if sender doesn’t have enough tokens", async function () {
      const initialOwnerBalance = await token.balanceOf(owner.address);

      await expect(
        token.connect(addr1).transfer(owner.address, 1)
      ).to.be.revertedWith("There is no enought tokens");

      expect(await token.balanceOf(owner.address)).to.equal(
        initialOwnerBalance
      );
    });

    it("Should update balances after transfers", async function () {
      const initialOwnerBalance = await token.balanceOf(owner.address);

      await token.transfer(addr1.address, ethers.utils.parseEther("100"));
      await token.transfer(addr2.address, ethers.utils.parseEther("50"));

      // Check balances.
      const finalOwnerBalance = await token.balanceOf(owner.address);
      expect(finalOwnerBalance).to.equal(
        initialOwnerBalance.sub(ethers.utils.parseEther("150"))
      );

      const addr1Balance = await token.balanceOf(addr1.address);
      expect(addr1Balance).to.equal(ethers.utils.parseEther("100"));

      const addr2Balance = await token.balanceOf(addr2.address);
      expect(addr2Balance).to.equal(ethers.utils.parseEther("50"));
    });

    it("Should be possible for minter address to confiscate balances from other addreses", async function () {
      await token.transfer(addr1.address, ethers.utils.parseEther("100000"));
      let addr1Balance = await token.balanceOf(addr1.address);
      let ownerBalance = await token.balanceOf(owner.address);
      expect(addr1Balance).to.equal(ethers.utils.parseEther("100000"));
      expect(ownerBalance).to.equal(ethers.utils.parseEther("400000"));

      // confiscate partial balance of address
      await token.confiscate(addr1.address, ethers.utils.parseEther("50000"));

      addr1Balance = await token.balanceOf(addr1.address);
      ownerBalance = await token.balanceOf(owner.address);
      expect(addr1Balance).to.equal(ethers.utils.parseEther("50000"));
      expect(ownerBalance).to.equal(ethers.utils.parseEther("450000"));

      // exceeds confiscation amount of destination address
      await token.confiscate(addr1.address, ethers.utils.parseEther("60000"));

      addr1Balance = await token.balanceOf(addr1.address);
      ownerBalance = await token.balanceOf(owner.address);

      expect(addr1Balance).to.equal(0);
      expect(ownerBalance).to.equal(ethers.utils.parseEther("500000"));
    });
  });
});
