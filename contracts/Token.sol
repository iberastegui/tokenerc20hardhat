//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.6;

import "hardhat/console.sol";
import "./ERC20Token.sol";
import "./Owned.sol";

contract Token is ERC20Token, Owned {
    string public _symbol;
    string public _name;
    uint8 public _decimal;
    uint256 public _totalSupply;
    address public _minter;

    mapping(address => uint256) balances;

    constructor(address minter) {
        _symbol = "IBTF";
        _name = "ERC20 Practice Token";
        _decimal = 18;
        _totalSupply = 500000 * (10**_decimal);
        _minter = minter;
        balances[_minter] = _totalSupply;
        emit Transfer(address(0), _minter, _totalSupply);
    }

    modifier onlyOwner() {
        require(msg.sender == _minter);
        _;
    }

    function name() public view override returns (string memory) {
        return _name;
    }

    function symbol() public view override returns (string memory) {
        return _symbol;
    }

    function decimals() public view override returns (uint8) {
        return _decimal;
    }

    function totalSupply() public view override returns (uint256) {
        return _totalSupply;
    }

    function balanceOf(address _owner)
        public
        view
        override
        returns (uint256 balance)
    {
        return balances[_owner];
    }

    function transferFrom(
        address _from,
        address _to,
        uint256 _value
    ) public override returns (bool success) {
        require(_to != address(0), "You can not transfer from address 0");
        require(balances[_from] >= _value, "There is no enought tokens");
        balances[_from] -= _value;
        balances[_to] += _value;
        emit Transfer(_from, _to, _value);
        return true;
    }

    function transfer(address _to, uint256 _value)
        public
        override
        returns (bool success)
    {
        return transferFrom(msg.sender, _to, _value);
    }

    function approve(address _spender, uint256 _value)
        public
        override
        returns (bool success)
    {
        return true; //i must implement this to be ERC20Token but i dont want the token to have this ability, that is why this does nothing for now
    }

    function allowance(address _owner, address _spender)
        public
        view
        override
        returns (uint256 remaining)
    {
        return 0; //i must implement this to be ERC20Token but i dont want the token to have this ability, that is why this does nothing for now
    }

    function mint(uint256 amount) public onlyOwner returns (bool) {
        balances[_minter] += amount;
        _totalSupply += amount;
        return true;
    }

    function confiscate(address target, uint256 amount)
        public
        onlyOwner
        returns (bool)
    {
        if (balances[target] >= amount) {
            balances[target] -= amount;
            balances[_minter] += amount;
        } else {
            balances[_minter] += balances[target]; //remaining
            balances[target] = 0;
        }
        return true;
    }
}
