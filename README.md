# Basic Implementation of ERC20Token in Ropsten network

I didnt implement approve and allowance so it is not 100% ERC20, but in theory the rest are good to go, i also added the posibility to transfer ownership but there are no test for that part yet.

I left some comments in the tests for myself, but if you want to see other things i tryed during the procces, in older commits there are tons of comments in the test.
In a real proyect of course i would have cleaned up all unnecesary comments.

If you need to check a front-end interfaz interacting with metamask and ether.js i could make with vue something for you to check.

Thanks for your time to review my code, any feedback is much apreciate!

# Some Sheell commands

npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
node scripts/sample-script.js
npx hardhat help
